terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  backend "s3" {
    bucket = "git-iac-bucket-chiaratti"
    key    = "terraform/state/terraform.tfstate"
    region = "us-east-2"
  }
}

provider "aws" {
  # Configuration options
}
